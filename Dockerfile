FROM debian:buster-slim

RUN apt-get update \
    && apt-get install -y python3 python3-yaml python3-requests

WORKDIR /build

COPY create-image-license-reference.py .
COPY create-lrmi-json-tag.py . 
COPY pandoc-preparation.sh .
COPY default-pandoc.css .

ENTRYPOINT ["./pandoc-preparation.sh"]
