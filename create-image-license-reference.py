#!/usr/bin/env python3

#%%
import yaml
import json
import requests
import re
import sys
from pathlib import Path

#%%
# See examples in : https://stackabuse.com/command-line-arguments-in-python/

# find wikimedia images
#course = open(sys.argv[1] + ".md", "rt")
course = open("course" + ".md", "rt")
text = course.read()
images = re.findall(r"!\[([^\]]*)\]\(([^\)]*)\)", text)
for treffer in images:
    description = treffer[0]
    link = treffer[1]
    print(link)
    if "wikimedia" in link:
        print("TREFFER for wikimedia: " + description + " " + link)
        image_name = re.findall(r"\/([^\/]*)$", link)[0]

        # get image_name metadata
        session = requests.Session()
        api_url = "https://en.wikipedia.org/w/api.php"
        params_image = {
            "action": "query",
            "format": "json",
            "prop" : "imageinfo",
            "iiprop": "user|userid|canonicaltitle|url|extmetadata",
            "titles": "File:" + image_name
        }

        image_data = session.get(url=api_url, params=params_image).json()
        # print(json.dumps(image_data, indent=4, ensure_ascii=0))

        IPAGES = image_data["query"]["pages"]

        for k, v in IPAGES.items():
            print("LIZENZ : " + v["imageinfo"][0]["extmetadata"]["LicenseShortName"]["value"])
            image_info = v["imageinfo"][0]
            image_title = image_info["extmetadata"]["ObjectName"]["value"]
            image_author = image_info["user"]
            image_author_link = "https://commons.wikimedia.org/wiki/User:" + image_author
            image_page = image_info["descriptionurl"]
            license_name = image_info["extmetadata"]["UsageTerms"]["value"]
            license_short_name = image_info["extmetadata"]["LicenseShortName"]["value"]
            if (license_short_name == "Public domain") :
                license_url = "https://creativecommons.org/publicdomain/zero/1.0/deed.de"
            else :
                license_url = image_info["extmetadata"]["LicenseUrl"]["value"]

            tullu = ("[" + image_title + "](" + image_page + ") von [" + image_author + "](" + image_author_link + ") unter ["  + license_short_name + "](" + license_url + ")")
            # print(tullu)

            # machine readable TULLU string
            mtullu = "<div about=\"" + link + "\" class=\"figure\"> \n <img src=\"" + link + "\" alt=\"" + image_title + "\" /> <br> \n <span property=\"dc:title\">" + image_title + "</span> \n von <a rel=\"cc:attributionURL dc:creator\" href=\"" + image_author_link + "\" property=\"cc:attributionName\">" + image_author + "</a>, Lizenz: <a rel=\"license\" href=\"" + license_url + "\">" + license_short_name + "</a>. Abrufbar unter: <a rel=\"dc:source\" href=\"" + link + "\">Wikimedia Commons</a> \n</div>"
            print(mtullu)

            # replace original image with image + citation
            text = re.sub(r"!\[" + description + r"\]\(" + link + r"\)", mtullu, text)
    
    # check for images with "image/" in link
    if "images/" in link:
        print("TREFFER for images: " + description + " " + link)

        # check if image metadata file exists
        image_meta_file_path = re.findall(r".*(?=\.)", link)[0] + ".meta"
        image_meta_file = Path(image_meta_file_path)
        if image_meta_file.exists():
            print(f"meta-file \"{image_meta_file}\" exists")

            # get the metadata
            with open(image_meta_file_path, "r") as image_metadata:
                try:
                    data = yaml.safe_load(image_metadata)
                except yaml.YAMLError as exc:
                    print(exc)
                
            # set "image_alt" as "image_title" if not defined
            if "image_alt" in data.keys() and data["image_alt"] is not None:
                print("image_alt key is there and is not None")
            else:
                print("Image_alt key not there, setting image_title as image_alt")
                data["image_alt"] = data["image_title"]
            print(f'image_alt key is: {data["image_alt"]}')
        
            # just create mtullu if necessary fields are defined
            necessary_keys = {
                "image_title",
                "image_author",
                "image_author_link",
                "license_url",
                "license_short_name",
                "dcsource",
                "sourcetext"
            }
            
            if all(k in data for k in necessary_keys):
                print("I got all necessary keys! Nice!")
                necessary_keys_present = True
                
                # check if imageadapted is there
                if "imageadapted" in data.keys() and data["imageadapted"] is not None:
                    print("imageadapted is there and is not None")
                    imageadapted_string = "Veränderung: " + data["imageadapted"]
                else:
                    imageadapted_string = ""

                # check if isBasedOn is there
                if "isBasedOn" in data.keys() and data["isBasedOn"] is not None:
                    print("isBasedOn is there and is not None")
                else:
                    data["isBasedOn"] = ""

                based_on_keys = {
                    "isBasedOn",
                    "original_title",
                    "original_author",
                    "original_license_url",
                    "original_license_short_name"
                }

                # Check if based_on_keys are there -> bigger modifications were made
                # as stated here https://wiki.creativecommons.org/wiki/Best_practices_for_attribution#This_is_a_good_attribution_for_material_you_modified_slightly
                # bigger modifications should be attributed in a certain way
                # the description of modifications is then not necessary any more
                if necessary_keys_present and all(k in data for k in based_on_keys):
                    # check if original_author_link is there
                    if "original_author_link" in data.keys() and data["original_author_link"] is not None:
                        print("original_author_link is there and is not None")
                        original_author_link_string = "<span property=\"author cc:attributionName\"><a rel=\"dc:creator\" href=\"" + data["original_author_link"] + "\">" + data["original_author"] + "</a></span >"
                    else:
                        original_author_link_string = "<span property=\"author cc:attributionName\">" + data["original_author"] + "</span >"

                    # create machine readable TULLU + V string
                    print("'based_on_keys' are present...")
                    print("creating mtullu-string for bigger modifications")
                    mtullu = ("<div vocab=\"http://schema.org/\" typeof=\"CreativeWork\" "
                        "about=\"" + link + "\" "
                        "class=\"figure\"> \n <img src=\"" + link + "\" "
                        "alt=\"" + data["image_alt"] + "\" /> "
                        "<br> \n "
                        "<span property=\"name\"> \""+ data["image_title"] + "\"</span> \n "
                        "ist eine Bearbeitung des Werkes "
                        "<span property=\"isBasedOn\" typeof=\"CreativeWork\">"
                            "<span property=\"url\" href=\"" + data["isBasedOn"] + "\"><a href=\"" + data["isBasedOn"] + "\">\"" + data["original_title"] + "\"</a></span>"
                            " von "
                            + original_author_link_string +
                            ". Lizenz: <a rel=\"license\" href=\"" + data["original_license_url"] + "\">" + data["original_license_short_name"] + "</a>. "
                        "</span>"
                        "\n "
                        "<span property=\"dc:title\">\"" + data["image_title"] + "\"</span> \n "
                        "von "
                        "<a rel=\"cc:attributionURL dc:creator\" href=\"" + data["image_author_link"] + "\" "
                        "property=\"cc:attributionName\">" + data["image_author"] +
                        "</a>, Lizenz: <a rel=\"license\" href=\"" + data["license_url"] + "\">" + data["license_short_name"] + "</a>. "
                        "Abrufbar unter: <a rel=\"dc:source\" href=\"" + data["dcsource"] + "\">" + data["sourcetext"] + ".</a>"
                        "<a href=\"" + data["isBasedOn"] + "\"></a> \n "
                        "</div>")
                else:
                    # create machine readable TULLU string
                    # as stated here https://wiki.creativecommons.org/wiki/Best_practices_for_attribution#This_is_a_good_attribution_for_material_you_modified_slightly
                    # slight modifications can be attributed with a comment
                    print("creating mtullu-string with slight or none modifications")
                    mtullu = ("<div about=\"" + link + "\" "
                        "class=\"figure\"> \n <img src=\"" + link + "\" "
                        "alt=\"" + data["image_alt"] + "\" /> "
                        "<br> \n "
                        "<span property=\"dc:title\">\"" + data["image_title"] + "\"</span> \n "
                        "von "
                        "<a rel=\"cc:attributionURL dc:creator\" href=\"" + data["image_author_link"] + "\" "
                        "property=\"cc:attributionName\">" + data["image_author"] +
                        "</a>, Lizenz: <a rel=\"license\" href=\"" + data["license_url"] + "\">" + data["license_short_name"] + "</a>. "
                        "Abrufbar unter: <a rel=\"dc:source\" href=\"" + data["dcsource"] + "\">" + data["sourcetext"] + ".</a>"
                        "<a rel=\"schema:isBasedOn\" href=\"" + data["isBasedOn"] + "\"></a> " + imageadapted_string + " \n</div>")
                print(mtullu)

                # replace original image with image + citation
                text = re.sub(r"!\[" + description + r"\]\(" + link + r"\)", mtullu, text)
            else:
                print("There are necessary keys missing, not creating mtullu")

                
#%%

with open("metadata.yml", 'r') as course_metadata:
    try:
        data = yaml.safe_load(course_metadata)
    except yaml.YAMLError as exc:
        print(exc)

    course_title = data["name"]
    if "url" in data :
        course_url = data["url"]
    else :
        course_url = ""
    course_author = data["creator"]["givenName"] + " " + data["creator"]["familyName"]
    # course_author_url =

    if "contributors" in data:
        course_contributors = ""
        for contributor in data["contributor"]:
            course_contributors = course_contributors + contributor["givenName"] + " " + contributor["familyName"] + ", "
        print(course_contributors[:-2])
        course_contributors = course_contributors[:-2]
        course_contributors_string = " und" + course_contributors
    else:
        course_contributors_string = ""

    course_license_url = data["license"]
    if "public-domain" in course_license_url or "zero" in course_license_url :
        course_license_short_name = "Public domain"
    else :
        course_license_components = re.findall(r"licenses\/([^\/]*)\/([^\/]*)", course_license_url)
        course_license_code = course_license_components[0][0]
        course_license_version = course_license_components[0][1]
        course_license_short_name = "CC " + course_license_code.upper() + " " + course_license_version

    course_license_text = "---\n## Hinweis zur Nachnutzung\nDieses Werk und dessen Inhalte sind - sofern nicht anders angegeben - lizenziert unter " + course_license_short_name + ". Nennung gemäß [TULLU-Regel](https://open-educational-resources.de/oer-tullu-regel/) bitte wie folgt: " + "\"[" + course_title + "](" + course_url + ")\" von " + course_author + course_contributors_string +  ", Lizenz: [" + course_license_short_name + "](" + course_license_url + "). Die Quellen dieses Werks sind verfügbar auf [GitLab](" + course_url + ")."
    print(course_license_text)
#%%
with open(sys.argv[1] + "-tagged.md", 'w', encoding='utf8') as course_tagged:
    course_tagged.write(text + "\n\n" + course_license_text)

course.close()
