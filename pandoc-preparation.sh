#!/bin/sh

MD_FILE=$1
WORKDIR=/build

if [ ! -f "$MD_FILE".md ]
then
	echo "markdown file '$MD_FILE'.md not found."
	exit 1
elif [ ! -f "metadata.yml" ]
then
	echo "'metadata.yml' not found."
	exit 1
fi

if [ ! -f "pandoc.css" ]
then
	echo "Using default pandoc.css"
	cp $WORKDIR/default-pandoc.css pandoc.css
fi

echo "url: "$CI_PROJECT_URL >> metadata.yml
date -u +"datePublished: %Y-%m-%d %H:%M" >> metadata.yml
python3 $WORKDIR/create-image-license-reference.py $MD_FILE
python3 $WORKDIR/create-lrmi-json-tag.py

sed -e ':a' -e 'N' -e '$!ba' -e "s/\`\`\`math\n\([^$]*\)\n\`\`\`/\$\$\1\$\$/g" $MD_FILE-tagged.md > pd-preparation-tempfile1.md
sed -e ':a' -e 'N' -e '$!ba' -e "s/\\$\`\([^\`]*\)\`\\$/\$\1\$/g" pd-preparation-tempfile1.md > $MD_FILE-prepared.md

rm pd-preparation-tempfile1.md
