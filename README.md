# Course Pandoc Preparation

Docker container for preparation of pandoc conversion of git courses for OER repositories and Google Search.

* course format: markdown
* creates lrmi tags in _metadata.json_ for html output (https://www.dublincore.org/specifications/lrmi/lrmi_1/)
* creates _title.txt_ with metadata
* converts gitlab math formulas to latex math formulas
* uses default _pandoc.css_, if no _pandoc.css_ exists
* prepared course will be available in _course-prepared.md_
* images from wikimedia commons will get automatically a license notice
* images stored in an `image`-folder can be provided with metadata in the following way:
  - create a file with the same name as image, but with `.meta` extension (e.g. for `cat.jpg` it is `cat.meta`). The following attributes are available (with example values):
  ```yaml
  image_title: Brown Cat With Green Eyes
  image_author: Kelvin Valerio
  image_author_link: https://www.pexels.com/@kelvin809
  license_url: https://creativecommons.org/publicdomain/zero/1.0/
  license_short_name: CC0
  dcsource: https://www.pexels.com/photo/adorable-animal-blur-cat-617278/
  sourcetext: Pexels
  # additional alt text, otherwise use image_title
  image_alt: Eine schöne Katze
  # for information about modification/provenance 
  imageadapted: Größe verringert.
  
  # indicate special permissions/disclaimers
  # information about provenance of image should be provided if image got adapted (schema:isBasedOnUrl)
  isBasedOn: https://www.pexels.com/photo/adorable-animal-blur-cat-617278/

  # if these fields are filled out, there will be additional information text generated
  # to acknoledge to the original source
  original_title: Original Titel

  original_author: Original Autor

  original_author_link: https://www.original-autor.de

  original_license_url: https://creativecommons.org/publicdomain/zero/1.0/

  original_license_short_name: CC0

  # indicate special permissions/disclaimers (cc:morePermissions)
  permit:

  # reproduce copyright notice of source
  copyright:
  ```

* license notice for overall content is added to the end of the document, depending on the license in _metadata.yml_

Example project: https://gitlab.com/TIBHannover/oer/course-metadata-test

## Input
 * course in markdown-format
 * meta-data in file _metadata.yml_
     * mandatory fields: _license_, _creator -> givenName_, _creator -> familyName_, _name_, _inLanguage_

## Usage (CLI)

Use your current directory as a docker voulme, that includes your markdown course and your _metadata.yml_.

```
docker run -it --volume "`pwd`:/build" registry.gitlab.com/tibhannover/oer/course-pandoc-preparation <your-markdown-course>
```

## Usage (GitLab-CI)

Your gitlab project has to contain your markdown course, your _metadata.yml_ and a _.gitlab-ci.yml_

Example _.gitlab-ci.yml_:
```
preparepandoc:
  image:
    name: registry.gitlab.com/tibhannover/oer/course-pandoc-preparation
    entrypoint: [""]
  stage: build
  script:
    - /build/pandoc-preparation.sh <your-markdown-course-file>
  artifacts:
    untracked: true
    expire_in: 5min

pages:
  image:
    name: pandoc/latex:2.7
    entrypoint: [""]
  stage: deploy
  dependencies:
    - preparepandoc
  script:
    - pandoc title.txt course-prepared.md -f markdown -t epub3 -s -o course.epub --metadata pagetitle="Kursbeispiel" --webtex
    - pandoc title.txt course-prepared.md -f markdown -t latex -s -o course.pdf --metadata pagetitle="Kursbeispiel" --webtex
    - pandoc title.txt course-prepared.md -f markdown -t html -s -o index.html --metadata pagetitle="Kursbeispiel" --include-in-header=metadata.json --katex --css pandoc.css
    - pandoc title.txt course-prepared.md -f markdown -t asciidoc -s -o course.asc --metadata pagetitle="Kursbeispiel" --webtex
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
  - master

```
